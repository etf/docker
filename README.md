# ETF
Experiments Test Framework (ETF) is a measurement middleware for functional/availability testing of the resources. It’s primary purpose is testing the services to determine availability delivered by resource providers, but it has been successfully used in many deployment and software campaigns, security testing, evaluation of new technologies as well as a generic collector (crawler). It has the following features:

-    generic framework based on [Nagios-core](http://www.nagios.org) and [check_mk](https://mathias-kettner.de/check_mk.html)
-    pluggable interface for tests based on monitoring plugins standard
-    wide range of available plugins for grid infrastructure testing
-    python API for auto-generating nagios-core configuration from custom topologies and support for quick turn around in plugin-to-test cycle
-    site notifications/alerts, on-demand test re-scheduling and various interfaces to help with debugging

More information is available in the official documentation at http://etf.cern.ch/docs/latest/
The configuration is done via the [ETF hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-etf).