export GLITE_LOCATION="/usr"
export GLOBUS_TCP_PORT_RANGE="20000,25000"
export GLITE_LOCATION_VAR="/var"
export GLOBUS_HOSTNAME="`hostname`"
export MYPROXY_SERVER="myproxy.cern.ch"
export LCG_LOCATION="/usr"
export GRID_ENV_LOCATION="/usr/libexec"
export LCG_GFAL_INFOSYS="top-bdii.cern.ch:2170"
export GLOBUS_FTP_CLIENT_IPV6="true"
export GLOBUS_IO_IPV6="true"

if [[ -f /opt/omd/sites/etf/.oidc-agent/oidc-env.sh ]]; then
  source /opt/omd/sites/etf/.oidc-agent/oidc-env.sh
fi