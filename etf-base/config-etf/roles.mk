roles.update(
{'admin': {'alias': 'Administrator', 'builtin': True, 'permissions': {}},
 'guest': {'alias': u'Guest user',
           'builtin': True,
           'permissions': {'action.acknowledge': True,
                           'action.addcomment': True,
                           'action.reschedule': True,
                           'general.logout': False}},
 'user': {'alias': u'Normal monitoring user',
          'builtin': True,
          'permissions': {'action.enablechecks': False,
                          'general.change_password': False,
                          'general.logout': False,
                          'general.see_all': True}}})
